import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../views/restaurant/TableView.vue"),
      meta: {
        layout: "MainView",
        title: "Table",
        requiresAuth: true,
      },
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
      meta: {
        layout: "MainView",
        requiresAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/LoginView.vue"),
      meta: {
        layout: "FullLayout",
        title: "Login",
      },
    },
    {
      path: "/user",
      name: "user",
      component: () => import("../views/user/UserView.vue"),
      meta: {
        layout: "MainView",
        title: "User",
        requiresAuth: true,
      },
    },
    {
      path: "/chefview",
      name: "chefview",
      component: () => import("../views/restaurant/ChefView.vue"),
      meta: {
        layout: "MainView",
        title: "Chef",
        requiresAuth: true,
      },
    },
    {
      path: "/serveview",
      name: "serveview",
      component: () => import("../views/restaurant/ServeView.vue"),
      meta: {
        layout: "MainView",
        title: "Serve",
        requiresAuth: true,
      },
    },
    {
      path: "/order",
      name: "order",
      component: () => import("../views/restaurant/OrderView.vue"),
      meta: {
        layout: "MainView",
        title: "Ordered",
        requiresAuth: true,
      },
    },
    {
      path: "/menu/:id",
      name: "menu",
      component: () => import("../views/restaurant/MenuView.vue"),
      meta: {
        layout: isLogin() ? "MainView" : "FullLayout",
        title: "Menu",
      },
    },
    {
      path: "/reserve/:id",
      name: "ReserveTable",
      component: () => import("../views/restaurant/ReserveTable.vue"),
      meta: {
        layout: "FullLayout",
        title: "Reserve",
        requiresAuth: true,
      },
    },
    {
      path: "/QRCode/:id",
      name: "QRCode",
      component: () => import("../views/restaurant/QRCodeView.vue"),
      meta: {
        layout: "FullLayout",
        title: "QRCode",
        requiresAuth: true,
      },
    },
    {
      path: "/table",
      name: "table",
      component: () => import("../views/restaurant/TableView.vue"),
      meta: {
        layout: "MainView",
        title: "Table",
        requiresAuth: true,
      },
    },
    {
      path: "/cashier/:id",
      name: "cashier",
      component: () => import("../views/restaurant/CashierView.vue"),
      meta: {
        layout: "MainView",
        title: "Cahier",
        requiresAuth: true,
      },
    },
    {
      path: "/tablemanagement",
      name: "tablemanagement",
      component: () => import("../views/crud/TableManageView.vue"),
      meta: {
        layout: "MainView",
        title: "Table Management",
        requiresAuth: true,
      },
    },
    {
      path: "/productmanagement",
      name: "productmanagement",
      component: () => import("../views/crud/ProductManageView.vue"),
      meta: {
        layout: "MainView",
        title: "Product Management",
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach((to) => {
  document.title = `JADDJIB - ${to.meta.title}`;
  const link = document.querySelector("[rel='icon']");
  link!.setAttribute(
    "href",
    "https://cdn.discordapp.com/attachments/746298215124697181/1094912288823185558/logo.png"
  );
});

function isLogin() {
  const user = localStorage.getItem("user");
  if (user) {
    return true;
  }
  return false;
}

router.beforeEach((to) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
