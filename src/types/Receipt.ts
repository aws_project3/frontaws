import type Order from "./Order";
import type User from "./User";

export default interface Receipt {
  id?: number;
  total_price: number;
  discount: number;
  cash: number;
  change: number;
  employeeId?: number;
  orderId?: string;
}
