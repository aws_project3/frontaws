import type Table from "@/types/Table";
import http from "./axios";

function getTables() {
  return http.get("/tables");
}
function saveTable(table: Table) {
  return http.post("/tables", table);
}

function updateTable(table: Table) {
  return http.patch(`/tables/${table.id}`, table);
}

function deleteTable(id: number) {
  return http.delete(`/tables/${id}`);
}

export default { getTables, saveTable, updateTable, deleteTable };
