export default interface Menu {
  id?: number;
  name: string;
  price: number;
  img: string;
  qty?: number;
}
