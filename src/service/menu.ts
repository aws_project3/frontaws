import type Menu from "@/types/Menu";
import http from "./axios";

function getProducts() {
  return http.get("/products");
}

function getProductsById(id: number) {
  return http.get(`/products/${id}`);
}

function saveProduct(menu: Menu) {
  return http.post("/products", menu);
}

function updateProduct(id: number, menu: Menu) {
  return http.patch(`/products/${id}`, menu);
}

function deleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}

export default {
  getProducts,
  saveProduct,
  updateProduct,
  deleteProduct,
  getProductsById,
};
