import type User from "@/types/User";
import http from "./axios";

function getUsers() {
  return http.get("/employees");
}

function saveUser(user: User) {
  return http.post("/employees", user);
}

function updateUser(id: number, user: User) {
  return http.patch(`/employees/${id}`, user);
}

function deleteUser(id: number) {
  return http.delete(`/employees/${id}`);
}

export default { getUsers, saveUser, updateUser, deleteUser };
