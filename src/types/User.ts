export default interface User {
  id?: number;
  username: string;
  password: string;
  name: string;
  role: string;
  phone: string;
  salary: number;
}
